.PHONY: publish
publish:
	@git fetch --tags
	@echo latest 3 tags
	@git --no-pager tag | head -n 3
	@read -p "input your new release version (start with v): " version; \
		echo Your new version is: pkg/authentication/$$version; \
		git tag -a pkg/authentication/$$version -m "Release $$version" ;\
		git push --tags


