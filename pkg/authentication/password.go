package authentication

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/pbkdf2"
	"gorm.io/gorm"
)

const alg = "pbkdf2_sha256"

// const iterations = 150000
const dklen = 32

type User struct {
	ID          int
	Username    string
	Password    string
	IsSuperuser bool
	FirstName   string
	LastName    string
	Email       string
	IsStaff     bool
	IsActive    bool
	DateJoined  time.Time
}

func (User) TableName() string {
	return "auth_user"
}

func (u *User) CreateNew(connection *gorm.DB) {

	if u.Username == "" {
		u.Username = u.Email
	}

	res := connection.Create(u)
	if res.Error != nil {
		logrus.Fatal("Cannot create user: ", res.Error.Error())
	}
}

/*
* Verify password by comparing plain text password and hashed password
* in database
 */
func VerifyPassword(username, password string, connection *gorm.DB) *User {
	var user User
	connection.Where("username = ?", username).First(&user)

	if user.ID == 0 {
		return nil
	}

	splitted := strings.Split(user.Password, "$")
	iterations, _ := strconv.Atoi(splitted[1])
	salt := splitted[2]
	if user.Password == HashPassword(salt, password, iterations) {
		return &user
	}

	return nil
}

func HashPassword(salt string, password string, iterations int) string {
	hash := pbkdf2.Key([]byte(password), []byte(salt), iterations, dklen, sha256.New)
	encoded := base64.StdEncoding.EncodeToString(hash)

	pass := fmt.Sprintf("%s$%d$%s$%s", alg, iterations, salt, encoded)
	return pass
}
