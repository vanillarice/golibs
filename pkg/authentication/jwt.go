package authentication

import (

	// "fmt"
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/99designs/gqlgen/graphql"
	jwt "github.com/appleboy/gin-jwt/v2"
	jwtLib "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/vektah/gqlparser/gqlerror"
	"gorm.io/gorm"
)

type JwtClaim struct {
	UserID   int
	UserName string
}
type credential struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

const identityKey = "username"

func GetJwtMiddleware(timeoutInHours int, secretKey, realm string, db *gorm.DB) *jwt.GinJWTMiddleware {
	timeout := time.Hour * time.Duration(timeoutInHours)

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:            realm,
		SigningAlgorithm: "HS256",
		Key:              []byte(secretKey),
		Timeout:          timeout,
		MaxRefresh:       time.Hour,
		IdentityKey:      identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*JwtClaim); ok {
				// fmt.Println(v)
				return jwt.MapClaims{
					// "user_id": 1,
					"user_id":  v.UserID,
					"username": v.UserName,
				}
			}
			return jwt.MapClaims{}
		},
		/*
					IdentityHandler: func(c *gin.Context) interface{} {
						claims := jwt.ExtractClaims(c)
						return &JwtClaim{
							UserID:   "1",
							UserName: claims[identityKey].(string),
							Email:    "conanlics@gmail.com",
						}
					},
			Authorizator: func(data interface{}, c *gin.Context) bool {
				if v, ok := data.(*JwtClaim); ok && v.UserName == "admin" {
					return true
				}

				return false
			},
		*/
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var creds credential
			if err := c.ShouldBind(&creds); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			username := creds.Username
			password := creds.Password
			user := VerifyPassword(username, password, db)

			if user != nil {
				return &JwtClaim{
					UserID:   user.ID,
					UserName: user.Username,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})
	if err != nil {
		panic("JWT middleware failed" + err.Error())
	}
	return authMiddleware
}

type MyCustomClaims struct {
	UserID int `json:"user_id"`
	jwtLib.StandardClaims
}

func GetUserIdFromContext(ctx context.Context) (int, error) {
	gc, err := ginContextFromContext(ctx)
	tokenString := strings.Split(gc.Request.Header.Get("Authorization"), " ")[1]

	token, err := jwtLib.ParseWithClaims(tokenString, &MyCustomClaims{}, func(token *jwtLib.Token) (interface{}, error) {
		return []byte("t@r9#f@r^hg4j#5%hfqgx&7vyxtsbzpf9a52zr6@=oq-t&(2_3"), nil
	})

	if claims, ok := token.Claims.(*MyCustomClaims); ok && token.Valid {
		logrus.Debugf("ok: %t; Claim: %+v\n", ok, *claims)
		return claims.UserID, nil
	}

	// Add 401 to graphql errors
	logrus.Error("not authenticated")
	graphql.AddError(ctx, &gqlerror.Error{
		Message: err.Error(),
		Extensions: map[string]interface{}{
			"code": "401",
		},
	})

	logrus.Error(err.Error())
	return 0, err
}

func ginContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value("GinContextKey")
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}
