module gitlab.com/vanillarice/golibs/pkg/authentication

go 1.14

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.8.0
	github.com/vektah/gqlparser v1.3.1
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	gorm.io/gorm v1.20.12
)
